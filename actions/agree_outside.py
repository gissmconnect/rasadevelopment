agree_outside = [
    {
        "major_type": "Outside Oman",
        "major_name": "  الفيزياء",
        "major_option": 1,
        "university_available": [
            {
                "university_name": "Dublin Institute of Technology",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علم تكنولوجيا النانو",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "الجامعة المستنصرية",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "فيزياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الكويت",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الفيزياء الهندسية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة عدن",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "فيزياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة قطر",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "فيزياء",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": " الأحياء",
        "major_option": 2,
        "university_available": [
            {
                "university_name": "Institute of Technology Carlow",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "العلوم البيولوجية مع الصيدلة الحيوية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "Kent State University",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التقنيات الحيوية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "أحياء",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "National University of Irelnd Maynooth",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "العلوم الحيوية والطب الحيوي",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "The University of Nebraska-Omaha",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التكنولوجيا الحيوية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "University of Glasgow",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علم الاحياء البشري( Human Biology )",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "West Virginia University",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الاحياء (Biology)",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "الجامعة اللبنانية الامريكية",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علم الاحياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة 6 اكتوبر",
                "university_option": 8,
                "subspeciality_detail": [
                    {
                        "subspeciality": "أحياء",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "بيولوجيا",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة البصرة/كلية العلوم الصحية",
                "university_option": 9,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الحياة",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الكويت",
                "university_option": 10,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الميكروبيولوجيا",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة حضرموت للعلوم والتكنولوجيا",
                "university_option": 11,
                "subspeciality_detail": [
                    {
                        "subspeciality": "أحياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة فيلادلفيا",
                "university_option": 12,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التكنولوجيا الحيوية وهندسة الجينات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة قطر",
                "university_option": 13,
                "subspeciality_detail": [
                    {
                        "subspeciality": "العلوم البيولوجية ",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة كارديف",
                "university_option": 14,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التكنولوجيا الحيوية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة ليدز",
                "university_option": 15,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علم الوراثة",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": " التاريخ",
        "major_option": 3,
        "university_available": [
            {
                "university_name": "جامعة الامارات العربية المتحدة",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التاريخ",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة البحرين",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اداب- تاريخ",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة العلوم والتكنولوجيا (اليمن)",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التاريخ",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الكويت",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التاريخ",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة اليرموك",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التاريخ",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "اداب- تاريخ",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة حضرموت للعلوم والتكنولوجيا",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التاريخ",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "اداب- تاريخ",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة قطر",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التاريخ",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "اداب- تاريخ",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة مؤته",
                "university_option": 8,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التاريخ",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": " التربية الإسلامية",
        "major_option": 4,
        "university_available": [
            {
                "university_name": "الجامعة الإسلامية بالمدينة المنورة",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الشريعة",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الحديث الشريف والدراسات الإسلامية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "الجامعة الاسلامية بالمدينة المنورة",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الدعوة الاسلامية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "الجامعة القاسمية",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الشريعة والدراسات الإسلامية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "المعهد العالي للتعليم التطبيقي والتدريب",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التربية الاسلامية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الكويت",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الدراسات الإسلامية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "التربية الاسلامية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة حضرموت للعلوم والتكنولوجيا",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التربية الاسلامية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة قطر",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الدراسات الإسلامية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية الامام مالك للشريعة والقانون",
                "university_option": 8,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الشريعة",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية الدراسات الاسلامية والعربية بدبي",
                "university_option": 9,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الدراسات الإسلامية",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": " الجغرافيا",
        "major_option": 5,
        "university_available": [
            {
                "university_name": "جامعة الامارات العربية المتحدة",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الجغرافيا",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "أداب- جغرافيا",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة الغرير",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التربية في الدراسات الاجتماعية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الكويت",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الجغرافيا",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة اليرموك",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الجغرافيا",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "أداب- جغرافيا",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة بيروت العربية",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الجغرافيا",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة حضرموت للعلوم والتكنولوجيا",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الجغرافيا",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "أداب- جغرافيا",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة قطر",
                "university_option": 8,
                "subspeciality_detail": [
                    {
                        "subspeciality": "أداب- جغرافيا",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": " الرياضيات",
        "major_option": 6,
        "university_available": [
            {
                "university_name": "الهيئة العامة للتعليم التطبيقي والتدريب",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "رياضيات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة إربد الأهلية",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "رياضيات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الامارات العربية المتحدة",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "رياضيات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الكويت",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "رياضيات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة جرش الأهلية",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "رياضيات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة حضرموت للعلوم والتكنولوجيا",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "رياضيات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة فيلادلفيا",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "رياضيات",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": " اللغة الإنجليزية",
        "major_option": 7,
        "university_available": [
            {
                "university_name": "Univevsity of Reading",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم اللغة وعلم النفس",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "الجامعة العثمانية (الهند)",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الإنجليزية وأدابها",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الامارات العربية المتحدة",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الانجليزية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "علم التطبيقات اللغوية tesol",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "التربية الانجليزية/ مسار اللغة الانجليزية",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "جامعة العين",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اعداد معلم اللغة الانجليزية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الكويت",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الانجليزية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة ظفار",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الانجليزية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة قطر",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "أدب إنجليزي ولغويات",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": " اللغة العربية",
        "major_option": 8,
        "university_available": [
            {
                "university_name": "الجامعة القاسمية",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة العربية وآدابها",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة أربد الأهلية",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة العربية وآدابها",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة جادارا",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة العربية وآدابها",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة جرش",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة العربية وآدابها",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة حضرموت للعلوم والتكنولوجيا",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة العربية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة فيلادلفيا",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة العربية وآدابها",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة قطر",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة العربية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية الدراسات الاسلامية والعربية بدبي",
                "university_option": 8,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة العربية وآدابها",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": " تقنية المعلومات",
        "major_option": 9,
        "university_available": [
            {
                "university_name": "Swinburne University of Technology",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الامن السيبراني",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "الجامعة العثمانية (الهند)",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم تقنية المعلومات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "الهيئة التطبيقية العامه للتدريب",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تكنولوجيا التعليم",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة ابوظبي",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تكنولوجيا نظم المعلومات- Information System Technology",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة جادارا",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تكنولوجيا التعليم",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية الشرق الأوسط لتقنية المعلومات",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تقنية الأنترنت- Internet Technology",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "نظم ادارة قواعد البيانات- Database Information Systems",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "مكونات الحاسب الألي والشبكات- Computer Hardware and Networking",
                        "subspeciality_option": 3
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": " رياضة مدرسية",
        "major_option": 10,
        "university_available": [
            {
                "university_name": "جامعة اليرموك",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية رياضية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة حضرموت للعلوم والتكنولوجيا",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية بدنية ورياضية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة حلوان",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية رياضية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة عدن",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية بدنية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة قطر",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية بدنية ورياضية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة مؤته",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية رياضية",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    
    {
        "major_type": "Outside Oman",
        "major_name": " مهارات موسيقية",
        "major_option": 11,
        "university_available": [
            {
                "university_name": "الاكاديمية الاردنية للموسيقى",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "العلوم الموسيقية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "المعهد العالي للفنون الموسيقية",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الفنون الموسيقية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة اليرموك",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "العلوم الموسيقية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة بنها",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التربية الموسيقية",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": "الفنون تشكيلية",
        "major_option": 12,
        "university_available": [
            {
                "university_name": "الكلية العلمية للتصميم",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الفنون الجميلة",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "المعهد العالي للفنون المسرحية",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الفنون الجميلة",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "الهيئة العامه للتعليم التطبيقي والتدريب",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية فنية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة البلقاء التطبيقية",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الخزف",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة القاهرة",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الفنون التشكيلية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة اليرموك",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الفنون الجميلة",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الفنون التشكيلية",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "فنون تشكيلية/خزف (سيراميك)",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "فنون تشكيلية/ التربية الفنية والثقافة البصرية",
                        "subspeciality_option": 4
                    }
                ]
            },
            {
                "university_name": "جامعة بنها",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية فنية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة حضرموت للعلوم والتكنولوجيا",
                "university_option": 8,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية فنية",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": "الكيمياء",
        "major_option": 13,
        "university_available": [
            {
                "university_name": "Bharti Vidyapeeth Deemed University",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "University of Bradford",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "CHEMISTRY",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "University of Hudders Field",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الكيمياء مع العلوم الجنائية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "University of Nebraska at Omaha",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "CHEMISTRY",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "University of Southampton",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الكيمياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الامارات العربية المتحدة",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة البحر الاحمر",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الصناعات الكيميائية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة البعث",
                "university_option": 8,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الكيمياء البحته",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الكويت",
                "university_option": 9,
                "subspeciality_detail": [
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الكيمياء الحيوية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة ام القرى",
                "university_option": 10,
                "subspeciality_detail": [
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة جرش الأهلية",
                "university_option": 11,
                "subspeciality_detail": [
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة حضرموت للعلوم والتكنولوجيا",
                "university_option": 12,
                "subspeciality_detail": [
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة قطر",
                "university_option": 13,
                "subspeciality_detail": [
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة كاريف",
                "university_option": 14,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الكيمياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 15,
                "subspeciality_detail": [
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية العلوم التطبيقية بالرستاق",
                "university_option": 16,
                "subspeciality_detail": [
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    }
]