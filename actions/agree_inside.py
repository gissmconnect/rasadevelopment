agree_inside = [
    {
        "major_type": "Inside Oman",
        "major_name": " الأحياء",
        "major_option": 1,
        "university_available": [
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "أحياء",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "العلوم الطبية الحيوية",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "علوم المختبرات الطبية",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "الاحياء التطبيقية",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "التقنيات الحيوية",
                        "subspeciality_option": 5
                    },
                    {
                        "subspeciality": "بيولوجيا بيئية",
                        "subspeciality_option": 6
                    }
                ]
            },
            {
                "university_name": "جامعة صحار",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "أحياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "أحياء",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "التقنيات الحيوية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة التقنية والعلوم التطبيقية",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التقنية الحيوية البحرية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "التقنية الحيوية البيئية",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "التقنية الحيوية للغذاء والزراعة",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "تقنية الزراعة والغذاء",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "أحياء",
                        "subspeciality_option": 5
                    },
                    {
                        "subspeciality": "الاحياء التطبيقية",
                        "subspeciality_option": 6
                    }
                ]
            },
            {
                "university_name": "كلية عمان الطبية",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "العلوم الصحية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية ولجات للعلوم التطبيقية",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة التكنولوجيا الحيوية",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": " التاريخ",
        "major_option": 2,
        "university_available": [
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التاريخ",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "اداب- تاريخ",
                        "subspeciality_option": 2
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": " التربية الإسلامية",
        "major_option": 3,
        "university_available": [
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التربية الاسلامية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية الشريعة والقانون",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الدراسات الإسلامية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية العلوم الشرعية",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الدراسات الإسلامية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الفقه والدعوة",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "الفقه وأصوله",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "اصول الدين",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "العلوم الشرعية",
                        "subspeciality_option": 5
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": " الجغرافيا",
        "major_option": 4,
        "university_available": [
        
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الجغرافيا",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "أداب- جغرافيا",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية العلوم التطبيقية بالرستاق",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الجغرافيا",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": " تقنية المعلومات",
        "major_option": 5,
        "university_available": [
            {
                "university_name": "الجامعة العربية المفتوحة",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الحوسبة /المسار التخصصي الاتصالات- Information Technology and Computing/communication",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "تقنية المعلومات والحوسبة- Information Technology and Computing",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "الكلية الحديثة لتجارة والعلوم",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "نظم المعلومات- Information System",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "أنظمة المعلومات- Information Systems",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "علوم الكمبيوتر- Computer Science",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تكنولوجيا التعليم والتعلم- Intructional and Learning Technologies",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الحاسب الالي (كلية العلوم)",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة صحار",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الشبكات وقواعد البيانات",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "نظم المعلومات للأعمال- Business Information Technology",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "علوم الحاسوب وهندسة الويب- Computer and Web Engineering",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "علوم الحاسوب والوسائط المتعددة- Computer and Multimedia",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "نظم المعلومات الادارية- Management Information Systems",
                        "subspeciality_option": 5
                    }
                ]
            },
            {
                "university_name": "جامعة ظفار",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تقنية المعلومات",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "علوم الكمبيوتر- Computer Science",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الكمبيوتر- Computer Science",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "هندسة الحاسوب- Computer Engineering",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "تصميم الويب وأمن المعلومات",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "نظم المعلومات- Information System",
                        "subspeciality_option": 4
                    }
                ]
            },
            {
                "university_name": "كليات ولجات للعلوم التطبيقية",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تطبيقات الكمبيوتر- Computer Application",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "هندسة علوم كمبيوتر- Engineering in Computer Sciences",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية البريمي",
                "university_option": 8,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الكمبيوتر- Computer Science",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "تكنولوجيا المعلومات- Information Technology",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية البريمي الجامعية",
                "university_option": 9,
                "subspeciality_detail": [
                    {
                        "subspeciality": "نظم المعلومات- Information System",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "علوم الحاسب الألي-Computer Science",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "أنظمة المعلومات- Information Systems",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "كلية الخليج",
                "university_option": 10,
                "subspeciality_detail": [
                    {
                        "subspeciality": "نظم المعلومات- Information System",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "برمجة الاتصالات",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "تقنية المعلومات",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "علوم الكمبيوتر- Computer Science",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "برمجة وتحليل نظم (mobile computing)",
                        "subspeciality_option": 5
                    }
                ]
            },
            {
                "university_name": "كلية الزهراء للبنات",
                "university_option": 11,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الكمبيوتر- Computer Science",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية الشرق الأوسط",
                "university_option": 12,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الحاسب الألي-Computer Science",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الشبكات اللاسلكية",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "تقنية البرمجيات",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "جامعة التقنية والعلوم التطبيقية",
                "university_option": 13,
                "subspeciality_detail": [
                    {
                        "subspeciality": "شبكات الحاسوب- Computer Networks ",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "أمن تقنية معلومات- IT- Security",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "تطويرالبرمجيات",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "أدارة البيانات- Data Management",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "شبكة الحاسوب/ دراسات الاتصال- Computer Networks/ Communication",
                        "subspeciality_option": 5
                    },
                    {
                        "subspeciality": "شبكات الحاسوب / التصميم- Computer Networks/ Design",
                        "subspeciality_option": 6
                    },
                    {
                        "subspeciality": "شبكات الحاسوب / إدارة الأعمال الدولية- Computer Networks/ International Business Administration",
                        "subspeciality_option": 7
                    },
                    {
                        "subspeciality": "أمن تقنية المعلومات/أدارة الأعمال الدولية- IT-Security/International Business Administration",
                        "subspeciality_option": 8
                    },
                    {
                        "subspeciality": "أمن تقنية المعلومات/التصميم-IT-Security/Design",
                        "subspeciality_option": 9
                    },
                    {
                        "subspeciality": "الشبكات- Networking",
                        "subspeciality_option": 10
                    },
                    {
                        "subspeciality": "الانترنت والامن الالكتروني",
                        "subspeciality_option": 11
                    },
                    {
                        "subspeciality": "نظم المعلومات- Information System",
                        "subspeciality_option": 12
                    },
                    {
                        "subspeciality": "قواعد بيانات- Database Specialization",
                        "subspeciality_option": 13
                    },
                    {
                        "subspeciality": "هندسة البرمجيات- Software Engineering",
                        "subspeciality_option": 14
                    },
                    {
                        "subspeciality": "هندسة الحاسوب- Computer Engineering",
                        "subspeciality_option": 15
                    }
                ]
            },
            {
                "university_name": "كلية صور الجامعية",
                "university_option": 14,
                "subspeciality_detail": [
                    {
                        "subspeciality": "نظم المعلومات- Information System",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "نظم وتكنولوجيا المعلومات(في نظم المعلومات)- Information System and Technology in Information System",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "نظم وتكنولوجيا المعلومات(في تكنولوجيا المعلومات)- Information System and Technology in Information  Technology",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "الحاسوب وتطبيقات الأنترنت- Computer and Internet Applications",
                        "subspeciality_option": 4
                    }
                ]
            },
            {
                "university_name": "كلية عمان للإدارة والتكنولوجيا",
                "university_option": 15,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الكمبيوتر- Computer Science",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "نظم المعلومات- Information System",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "نظم المعلومات الادارية",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "كلية كالودونيان الهندسية",
                "university_option": 16,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة كمبيوتر- Computer Engineering",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية مجان",
                "university_option": 17,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الشبكات",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الحاسوب وتطبيقات الأنترنت- Computer and Internet Applications",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "علوم الكمبيوتر- Computer Science",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "كلية مزون",
                "university_option": 18,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الكمبيوتر- Computer Science",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "علم تقنية المعلومات- Information Science & Technology",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "أدارة نظم المعلومات- Management Informatin System",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "كلية مزون للإدارة والعلوم التطبيقية",
                "university_option": 19,
                "subspeciality_detail": [
                    {
                        "subspeciality": "ادارة نظم المعلومات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية مسقط",
                "university_option": 20,
                "subspeciality_detail": [
                    {
                        "subspeciality": " الحاسب الألي للأعمال(business Computing)",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "علوم الحاسب الألي-Computer Science",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "المحاسبة والعلم الحاسوبي- Accountancy and Computing Science",
                        "subspeciality_option": 3
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": " رياضة مدرسية",
        "major_option": 6,
        "university_available": [
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية رياضية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة صحار",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية رياضية",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": " مادة الرياضيات",
        "major_option": 7,
        "university_available": [
            {
                "university_name": "الكلية الحديثة للتجارة والعلوم",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الاحصاء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "رياضيات",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الاحصاء",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة صحار",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "رياضيات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة ظفار",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "رياضيات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "رياضيات",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الاحصاء",
                        "subspeciality_option": 2
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": "الفنون تشكيلية",
        "major_option": 8,
        "university_available": [
            {
                "university_name": "الكلية العلمية للتصميم",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الفنون الجميلة",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية فنية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تربية فنية",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": "الفيزياء",
        "major_option": 9,
        "university_available": [
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "فيزياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "فيزياء",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية العلوم التطبيقية بالرستاق",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "فيزياء",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": "الكيمياء",
        "major_option": 10,
        "university_available": [
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الكيمياء التطبيقية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية العلوم التطبيقية بالرستاق",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "كيمياء",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "الكيمياء التطبيقية",
                        "subspeciality_option": 3
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": "اللغة الإنجليزية",
        "major_option": 11,
        "university_available": [
            {
                "university_name": "الجامعة العربية المفتوحة",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الإنجليزية وأدابها",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الإنجليزية وأدابها",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "اللغة الانجليزية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة صحار",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الإنجليزية وأدابها",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "اللغة الانجليزية",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "اللغة الانجليزية والترجمة",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "جامعة ظفار",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الانجليزية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الإنجليزية وأدابها",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "اللغة الانجليزية",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "اللغة الانجليزية والترجمة",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "كلية البريمي",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الإنجليزية وأدابها",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "اللغة الانجليزية والترجمة",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية البيان",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الإنجليزية وأدابها",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "ادب انجليزي",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية الزهراء للبنات",
                "university_option": 8,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الإنجليزية وأدابها",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية العلوم التطبيقية بالرستاق",
                "university_option": 9,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الانجليزية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية مجان",
                "university_option": 10,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الإنجليزية وأدابها",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية مزون",
                "university_option": 11,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الانجليزية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية مزون للإدارة والعلوم التطبيقية",
                "university_option": 12,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة الإنجليزية وأدابها",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": "اللغة العربية",
        "major_option": 12,
        "university_available": [
            {
                "university_name": "جامعة السلطان قابس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة العربية وآدابها",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة صحار",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة العربية وآدابها",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "اللغة العربية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة ظفار",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة العربية وآدابها",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "اللغة العربية وآدابها",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "اللغة العربية",
                        "subspeciality_option": 2
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": "مهارات موسيقية",
        "major_option": 13,
        "university_available": [
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الموسيقى والعلوم الموسيقية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة صحار",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التربية الموسيقية",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    }
]
