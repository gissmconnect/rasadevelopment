nonagree_inside = [
    {
        "major_type": "Inside Oman",
        "major_name": " الاحياء",
        "major_option": 1,
        "university_available": [
            {
                "university_name": "جامعة التقنية والعلوم التطبيقية",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم البيئة",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "هندسة المعدات الطبية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "الكلية الحديثة للتجارة والعلوم",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "ادارة الصحة والسلامة",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة البريمي",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم البصريات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علم النبات",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "علوم التربة",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "التغذية والغذائيات",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "علم الاغذية",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "التغذية البشرية",
                        "subspeciality_option": 5
                    },
                    {
                        "subspeciality": "إدارة تربه ومياه",
                        "subspeciality_option": 6
                    },
                    {
                        "subspeciality": "تقنيات المياه",
                        "subspeciality_option": 7
                    },
                    {
                        "subspeciality": "تقنية بيطرية",
                        "subspeciality_option": 8
                    },
                    {
                        "subspeciality": "علوم صحية",
                        "subspeciality_option": 9
                    },
                    {
                        "subspeciality": "علوم البحار والثروة السمكية",
                        "subspeciality_option": 10
                    },
                    {
                        "subspeciality": "اقتصاد الموارد الاقتصادية",
                        "subspeciality_option": 11
                    },
                    {
                        "subspeciality": "علم الحيوان",
                        "subspeciality_option": 12
                    },
                    {
                        "subspeciality": "التمريض",
                        "subspeciality_option": 13
                    },
                    {
                        "subspeciality": "الهندسة الزراعية",
                        "subspeciality_option": 14
                    }
                ]
            },
            {
                "university_name": "جامعة الشرقية",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الغذاء والتغذية البشرية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "صيدلة",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": " الرياضيات",
        "major_option": 2,
        "university_available": [
            {
                "university_name": "جامعة التقنية والعلوم التطبيقية",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة المعمارية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الهندسة المدنية",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "قواعد البيانات",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "هندسة الالكترونيات والاتصالات",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "ادارة مكاتب",
                        "subspeciality_option": 5
                    },
                    {
                        "subspeciality": "القوى الكهربائية",
                        "subspeciality_option": 6
                    },
                    {
                        "subspeciality": "هندسة المعدات الطبية",
                        "subspeciality_option": 7
                    },
                    {
                        "subspeciality": "هندسة الحاسوب",
                        "subspeciality_option": 8
                    },
                    {
                        "subspeciality": "محاسبة",
                        "subspeciality_option": 9
                    },
                    {
                        "subspeciality": "إدارة الموارد البشرية",
                        "subspeciality_option": 10
                    },
                    {
                        "subspeciality": "مسح كميات",
                        "subspeciality_option": 11
                    },
                    {
                        "subspeciality": "الهندسة الميكانيكية",
                        "subspeciality_option": 12
                    },
                    {
                        "subspeciality": "ادارة الاعمال الدولية",
                        "subspeciality_option": 13
                    },
                    {
                        "subspeciality": "المحاسبة",
                        "subspeciality_option": 14
                    },
                    {
                        "subspeciality": "ادارة البيانات",
                        "subspeciality_option": 15
                    }
                ]
            },
            {
                "university_name": "جامعة البريمي",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة المعلومات والاتصالات",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "تنمية الموارد البشرية",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "الهندسة المدنية",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الحاسب الالي",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "الاحصاء التجاري",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "المالية/التسويق",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "الاتصالات ومعالجة الاشارات",
                        "subspeciality_option": 5
                    },
                    {
                        "subspeciality": "ادارة العمليات",
                        "subspeciality_option": 6
                    },
                    {
                        "subspeciality": "المحاسبة",
                        "subspeciality_option": 7
                    },
                    {
                        "subspeciality": "الادارة",
                        "subspeciality_option": 8
                    },
                    {
                        "subspeciality": "هندسة الميكاترونيكس",
                        "subspeciality_option": 9
                    },
                    {
                        "subspeciality": "اقتصاد الموارد الطبيعية",
                        "subspeciality_option": 10
                    },
                    {
                        "subspeciality": "الهندسة الزراعية",
                        "subspeciality_option": 11
                    },
                    {
                        "subspeciality": "الهندسة الصناعية",
                        "subspeciality_option": 12
                    },
                    {
                        "subspeciality": "الاقتصاد ",
                        "subspeciality_option": 13
                    }
                ]
            },
            {
                "university_name": "جامعة صحار",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الحاسوب والوسائط المتعددة",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الشبكات وقواعد البيانات",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "هندسة كيميائية",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "جامعة ظفار",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "المحاسبة",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "المحاسبة",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "ادارة الاعمال",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "هندسة الحاسوب",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "كلية البريمي الجامعية",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "المحاسبة",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية الخليج",
                "university_option": 8,
                "subspeciality_detail": [
                    {
                        "subspeciality": "المحاسبة",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية الدراسات المصرفية والمالية",
                "university_option": 9,
                "subspeciality_detail": [
                    {
                        "subspeciality": "المحاسبة",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية الزهراء للبنات",
                "university_option": 10,
                "subspeciality_detail": [
                    {
                        "subspeciality": "العلوم المالية والمصرفية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "ادارة الاعمال",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية الشرق الاوسط",
                "university_option": 11,
                "subspeciality_detail": [
                    {
                        "subspeciality": "ادارة الاعمال ونظم المعلومات (المحاسبة والمالية)",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "هندسة الالكترونيات والاتصالات",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "المحاسبة",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "مسح كميات وادارة انشاءات",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "مسح كميات",
                        "subspeciality_option": 5
                    }
                ]
            },
            {
                "university_name": "كلية صور الجامعية",
                "university_option": 12,
                "subspeciality_detail": [
                    {
                        "subspeciality": "إدارة اعمال في العلوم المالية والمصرفية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية عمان للادارة والتكنولوجيا",
                "university_option": 13,
                "subspeciality_detail": [
                    {
                        "subspeciality": "المحاسبة ",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "العلوم المالية والمصرفية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية كالدونيان الهندسية",
                "university_option": 14,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة ميكانيكية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الهندسة المدنية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية مجان",
                "university_option": 15,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التجارة الالكترونية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "المحاسبة",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية مزون",
                "university_option": 16,
                "subspeciality_detail": [
                    {
                        "subspeciality": "ادارة نظم المعلومات",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "ادارة الأعمال",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية مسقط",
                "university_option": 17,
                "subspeciality_detail": [
                    {
                        "subspeciality": "محاسبة ودراسات الأعمال",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية ولجات للعلوم التطبيقية",
                "university_option": 18,
                "subspeciality_detail": [
                    {
                        "subspeciality": "ادارة الاعمال",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": " الكيمياء",
        "major_option": 3,
        "university_available": [
            {
                "university_name": "الجامعة الالمانية للتكنولوجيا",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة العمليات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة الكيمياء والمعالجة التحويلية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "تقنيات المياه",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "علوم التربة",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "الهندسة المدنية",
                        "subspeciality_option": 4
                    }
                ]
            },
            {
                "university_name": "جامعة الشرقية",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الغذاء والتغذية الانسانية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة صحار",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة الكيميائية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة ظفار",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة الكيميائية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الهندسة الميكانيكية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة الكيميائية والبتروكيمائية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية الشرق الاوسط",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "مسح كميات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة التقنية والعلوم التطبيقية",
                "university_option": 8,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة الكيميائية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "هندسة القوى الكهربائية",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "الهندسة الكيميائية",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "كلية عمان البحرية الدولية",
                "university_option": 9,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة العمليات",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "تكنولوجيا عمليات التشغيل",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية عمان الطبية",
                "university_option": 10,
                "subspeciality_detail": [
                    {
                        "subspeciality": "صيدلة",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية كالدونيان الهندسية",
                "university_option": 11,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة الكيميائية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "هندسة المعالجة والصيانة",
                        "subspeciality_option": 2
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": " اللغة إنجليزية",
        "major_option": 4,
        "university_available": [
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الترجمة",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "إدارة السياحة",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية البريمي الجامعية",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الترجمة",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": " اللغة العربية",
        "major_option": 5,
        "university_available": [
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الصحافة والنشر الالكتروني",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "إدارة مؤسسات المعلومات",
                        "subspeciality_option": 2
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": "تقنية المعلومات",
        "major_option": 6,
        "university_available": [
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "نظم المعلومات (التجارة والاقتصاد)",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "نظم المعلومات(كلية الاقتصاد)",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "الهندسة الكهربائية والحاسب الالي (أنظمة الحاسب الالي والشبكات)",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "جامعة صحار",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة الكهرباء والحاسوب",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "ادارة الاعمال وتقنية المعلومات",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة ظفار",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "أدارة الأعمال-نظم المعلومات الادارية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "هندسة الكمبيوتر والاتصالات",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية الشرق الاوسط",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "ادارة الاعمال ونظم المعلومات",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة التقنية والعلوم التطبيقية",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الصحافة/ التصميم",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "إدارة الأعمال الإلكترونية",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "هندسة إلكترونيات واتصالات",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "دراسات الاتصال-الاعلام الرقمي",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "دراسات الاتصال-الاتصال الدولي",
                        "subspeciality_option": 5
                    },
                    {
                        "subspeciality": "دراسات الاتصال-الصحافة",
                        "subspeciality_option": 6
                    }
                ]
            },
            {
                "university_name": "كلية كالدونيان الهندسية",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة إتصالات",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": "التربية الاسلامية",
        "major_option": 7,
        "university_available": [
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "القانون",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "كلية البريمي الجامعية",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "القانون",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": "الجغرافيا",
        "major_option": 8,
        "university_available": [
            {
                "university_name": "الجامعة الالمانية للتكنولوجيا",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الأرض التطبيقية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الأرض",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "علم الاجتماع",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة التقنية والعلوم التطبيقية",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "دراسات الاتصال(العلاقات العامة-التصميم)",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "دراسات الاتصال(الاعلام الرقمي)",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "دراسات الاتصال(الاتصال الدولي)",
                        "subspeciality_option": 3
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": "الفنون التشكيلية",
        "major_option": 9,
        "university_available": [
            {
                "university_name": "الجامعة الالمانية للتكنولوجيا",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التخطيط الحضري والتصميم المعماري",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "الكلية العلمية للتصميم",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "تصميم ازياء",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "التصميم الجرافيكي- تصميم طباعي",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "التصميم الرقمي",
                        "subspeciality_option": 3
                    }
                ]
            },
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الفنون المسرحية (التصميم)",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة ظفار",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التصميم الجرافيكي",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "هندسة العمارة الداخلية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية الزهراء للبنات",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التصميم الجرافيكي",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة التقنية والعلوم التطبيقية",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الاتصال الدولي/ التصميم",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "التصميم الرقمي",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "التصميم الجرافيكي",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "التصميم الجرافيكي/دراسات الاتصال",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "التصميم الجرافيكي/إدارة الاعمال الدولية",
                        "subspeciality_option": 5
                    },
                    {
                        "subspeciality": "التصميم المكاني",
                        "subspeciality_option": 6
                    },
                    {
                        "subspeciality": "الاعلام الرقمي/التصميم",
                        "subspeciality_option": 7
                    },
                    {
                        "subspeciality": "التصميم المكاني",
                        "subspeciality_option": 8
                    }
                ]
            },
            {
                "university_name": "كلية عمان للادارة والتكنولوجيا",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التصميم الداخلي",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Inside Oman",
        "major_name": "الفيزياء",
        "major_option": 10,
        "university_available": [
            {
                "university_name": "جامعة التقنية والعلوم التطبيقية",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة المعمارية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "هندسة الحاسوب",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "الهندسة الميكانيكية",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "هندسة القوى الكهربائية",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "هندسة إلكترونيات واتصالات",
                        "subspeciality_option": 5
                    },
                    {
                        "subspeciality": "مسح كميات",
                        "subspeciality_option": 6
                    },
                    {
                        "subspeciality": "الهندسة المدنية",
                        "subspeciality_option": 7
                    },
                    {
                        "subspeciality": "هندسة الاتصالات",
                        "subspeciality_option": 8
                    },
                    {
                        "subspeciality": "هندسة المعدات الطبية",
                        "subspeciality_option": 9
                    },
                    {
                        "subspeciality": "الهندسة الكهربائية",
                        "subspeciality_option": 10
                    }
                ]
            },
            {
                "university_name": "جامعة السلطان قابوس",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة الكيمياء والمعالجة التحويلية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الهندسة الميكاترونية",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "جيوفيزياء",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "الهندسة الميكانيكية",
                        "subspeciality_option": 4
                    }
                ]
            },
            {
                "university_name": "جامعة الشرقية",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة المدنية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الهندسة الالكترونية والاتصالات",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة صحار",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة الكهرباء والحاسوب",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة ظفار",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة الكمبيوتر والاتصالات",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "هندسة ميكانيكية",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة نزوى",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة الكيميائية والبتروكيميائية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "علم العمارة",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية الشرق الاوسط",
                "university_option": 7,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة المدنية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "الالكترونيات والاتصالات",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "كلية كالدونيان الهندسية",
                "university_option": 8,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة المدنية",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "هندسة إلكترونيات ",
                        "subspeciality_option": 2
                    },
                    {
                        "subspeciality": "هندسة الكترونيات",
                        "subspeciality_option": 3
                    },
                    {
                        "subspeciality": "الهندسة الميكانيكية بمساعدة الكمبيوتر",
                        "subspeciality_option": 4
                    },
                    {
                        "subspeciality": "هندسة الميكاترونكس",
                        "subspeciality_option": 5
                    }
                ]
            }
        ]
    }
]