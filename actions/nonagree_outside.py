nonagree_outside = [
    {
        "major_type": "Outside Oman",
        "major_name": " الاحياء",
        "major_option": 1,
        "university_available": [
            {
                "university_name": "Brad Ford",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الطب الشرعي",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "Institute Technology Sligo",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التقنية الحيوية الطبية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "University of Bristol",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الطب الخلوي والجزيئي(العلوم الطبية الحيوية)",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "University of Leicester",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علم الوراثة الطبية (Medical Genetics )",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "الجامعة العثمانية",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "Micro Biology, Genetics, Chemistry",
                        "subspeciality_option": 1
                    },
                    {
                        "subspeciality": "Bio Technology, Zoology, Chemistry",
                        "subspeciality_option": 2
                    }
                ]
            },
            {
                "university_name": "جامعة الامارات العربية المتحدة",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "البساتين من الأغذية والزراعة",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": " الجغرافيا",
        "major_option": 2,
        "university_available": [
            {
                "university_name": "CARDIFF UNIVERSITY",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "علوم الارض",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "The Ohio University",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الجغرافيا، علم الأرصاد الجوية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة قطر",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الدراسات الاجتماعية",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": "التربية الاسلامية",
        "major_option": 3,
        "university_available": [
            {
                "university_name": "جامعة أربد الاهلية",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "القانون",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": "الرياضيات",
        "major_option": 4,
        "university_available": [
            {
                "university_name": "Dubline Institute technology",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الاقتصاد والمالية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "HERIOT-WATT (المملكة المتحدة)",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة كيميائية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الامارات العربية المتحدة",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "المحاسبة",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": "الفنون التشكيلية",
        "major_option": 5,
        "university_available": [
            {
                "university_name": "جامعة اربد",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "التصميم الجرافيكي",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": "الفيزياء",
        "major_option": 6,
        "university_available": [
            {
                "university_name": "CARDIFF UNIVERSITY",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة المعمارية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "Coventry University",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة الميكانيكية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة كارديف",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "هندسة معمارية",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": "الكيمياء",
        "major_option": 7,
        "university_available": [
            {
                "university_name": "Brindavan College",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الكيمياء، علم الحيوان والأحياء الدقيقة",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "The Oxford college",
                "university_option": 2,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الكيمياء الحيوية، علم الوراثة، التقنية الحيوية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "University Putra Malaysia",
                "university_option": 3,
                "subspeciality_detail": [
                    {
                        "subspeciality": "بكالوريوس الهندسة (كيمياء)",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "University of Bradford",
                "university_option": 4,
                "subspeciality_detail": [
                    {
                        "subspeciality": "العلوم الجنائية (Forensic Science)",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة الامارات العربية المتحدة",
                "university_option": 5,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة الكيميائية",
                        "subspeciality_option": 1
                    }
                ]
            },
            {
                "university_name": "جامعة قيتا",
                "university_option": 6,
                "subspeciality_detail": [
                    {
                        "subspeciality": "الهندسة الكيميائية",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": "اللغة إنجليزية",
        "major_option": 8,
        "university_available": [
            {
                "university_name": "جامعة بونا",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "أداب(إنجليزي)",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    },
    {
        "major_type": "Outside Oman",
        "major_name": "تقنية المعلومات",
        "major_option": 9,
        "university_available": [
            {
                "university_name": "جامعة العين",
                "university_option": 1,
                "subspeciality_detail": [
                    {
                        "subspeciality": "ادارة الاعمال في نظم المعلومات الادارية",
                        "subspeciality_option": 1
                    }
                ]
            }
        ]
    }
]